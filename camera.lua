camera = {}
camera._x = 0
camera._y = 0
camera._bounds = {}
camera.scaleX = 1
camera.scaleY = 1
camera.rotation = 0
camera.shakeTimer = 1
camera.shakeIntensity = 10
camera.isShaking = false

function camera:set()
  love.graphics.push()
  love.graphics.rotate(-self.rotation)
  love.graphics.scale(1 / self.scaleX, 1 / self.scaleY)
  love.graphics.translate(-self._x, -self._y)
end

function camera:unset()
  love.graphics.pop()
end

function camera:move(dx, dy)
  self._x = self._x + (dx or 0)
  self._y = self._y + (dy or 0)
end

function camera:rotate(dr)
  self.rotation = self.rotation + dr
end

function camera:scale(sx, sy)
  sx = sx or 1
  self.scaleX = self.scaleX * sx
  self.scaleY = self.scaleY * (sy or sx)
end

function camera:setX(value)
  if self._bounds then
    self._x = math.clamp(value, self._bounds.x1, self._bounds.x2)
  else
    self._x = value
  end
end

function camera:setY(value)
  if self._bounds then
    self._y = math.clamp(value, self._bounds.y1, self._bounds.y2)
  else
    self._y = value
  end
end

function camera:setPosition(x, y)
  if x then self:setX(x) end
  if y then self:setY(y) end
end

function camera:setScale(sx, sy)
  self.scaleX = sx or self.scaleX
  self.scaleY = sy or self.scaleY
end

function camera:getBounds()
  return unpack(self._bounds)
end

function camera:getPosition()
  return self._x, self._y
end

function camera:setBounds(x1, y1, x2, y2)
  self._bounds = { x1 = x1, y1 = y1, x2 = x2, y2 = y2 }
end

function camera:shake(intensity, duration)
  self.shakeIntensity = intensity
  self.shakeTimer = duration
  self.isShaking = true

  -- ADJUST BOUNDARIES
  --print("Setting new bounds")
  x1 = self._bounds.x1 - self.shakeIntensity
  x2 = self._bounds.x2 + self.shakeIntensity
  y1 = self._bounds.y1 - self.shakeIntensity
  y2 = self._bounds.y2 + self.shakeIntensity
  self:setBounds(x1, y1, x2, y2)
end

function camera:update(dt)

  if not self.isShaking then

    -- C A M E R A  U P D A T E
    camera:setPosition((player.act_x * TileSize) - WIDTH / 2, (player.act_y * TileSize) - HEIGHT / 2) -- safe for mac
    --camera:setPosition((player.x * TileSize) - WIDTH / 2, (player.y * TileSize) - HEIGHT / 2) -- safe for windows
  else

    -- C A M E R A  S H A K E
    self.shakeTimer = self.shakeTimer - (10 * dt)
    
    if self.shakeTimer > 0 then

      local x, y = camera:getPosition()
      
      x = x + math.random(-self.shakeIntensity, self.shakeIntensity)
      y = y + math.random(-self.shakeIntensity, self.shakeIntensity)
      
      camera:setPosition(x, y)

      else
        self.shakeTimer = 1
        self.isShaking = false

        camera:setBounds(0, 0, cameraBounds.x2, cameraBounds.y2)
        --print("Original bounds set")

      end

    end
end