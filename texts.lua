orcNames = {
    "Gludbog",    
    "Ulgha",    
    "K'bguk",    
    "D'ugh",
    "Tobdakh",
    "Zugrak",
    "Gromka",
    "Shukn'k",
    "Uglug"
}

playerNames = {
    "Frumorn Woodsoul",
    "Pruella Duskwalker",
    "Elice Pazuzukin",
    "Seaven Wyrmeye",
    "Yllac Greatbeast"
}

worldNames = {
    "Alima",
    "Yberag",
    "Eowaln"    
}

txtMisses = {
    "%s missed %s by a mile.",
    "%s slashed at %s wildly but missed.",
    "%s's attack was dodged by %s"
}

txtHits = {
    "%s brained %s.",
    "%s whupped %s.",
    "%s hammered %s.",
    "%s clobbered %s"
}

startings = {
    "kick goblin butt",
    "prod buttock",
    "chew gum and kick ass",
    "start something"
}

worldTypes = {"Mountains", "Desert", "Forest", "Snowy"}