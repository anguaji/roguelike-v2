-- GameState.lua
-- basic game state implementation

GameState = {}

-- GameState options: 
-- init, title, play, pause, gameover-win, gameover-lose, end

GameState.state = "init"

function GameState.update(state)
	GameState.state = state
end