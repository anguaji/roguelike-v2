effects = {}

function effects.spawn(x, y, time, size)
	local animation = {}

	animation.x = x + size / 2
	animation.y = y + size / 2
	animation.timer = time
	animation.size = size

	table.insert(effects, animation)
end

function effects.update(dt)
	local remEffect = {}
	for i,v in ipairs(effects) do
		v.timer = v.timer - (100 * dt)
		v.size = v.size + 2
		if v.timer < 0 then
			table.insert(remEffect, i)
		end
	end

	for i,v in ipairs(remEffect) do
		table.remove(effects, v)
	end
end

function effects.draw()
	for i,v in ipairs(effects) do
		love.graphics.setColor(229, 76, 109, 255)
		love.graphics.circle("fill", v.x - v.size / 2, v.y - v.size / 2, v.size, v.size)
		-- the x and y compensate for the origin not being the center
	end
end

function particleEmit(x, y)

	particles:reset()
    particles:setPosition(x, y)
    particles:setSizes( 2, 1, 0 )
    --[[particles:setSpeed( 100, 200  )
    particles:setSpread( math.rad(40) )
    --]]
    particles:setEmissionRate( 50 )
    particles:setEmitterLifetime( 0.1 )
    particles:setParticleLifetime( 0.7 )--]]
    particles:setColors( 255, 0, 255, 255, 255, 0, 255, 255 )
    particles:start()
end