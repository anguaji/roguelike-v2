effects = {}

function effects.spawn(x, y, time, size)
	local animation = {}

	animation.x = x + size / 2
	animation.y = y + size / 2
	animation.timer = time
	animation.size = size

	table.insert(effects, animation)
end

function effects.update(dt)
	local remEffect = {}
	for i,v in ipairs(effects) do
		v.timer = v.timer - (100 * dt)

		if v.timer < 0 then
			table.insert(remEffect, i)
		end
	end

	for i,v in ipairs(remEffect) do
		table.remove(effects, v)
	end
end

function effects.draw()
	for i,v in ipairs(effects) do
		love.graphics.setColor(255, 255, 255, 255)
		love.graphics.circle("fill", v.x, v.y, v.size, v.size)
	end
end