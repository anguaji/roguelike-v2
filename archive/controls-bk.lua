keymovement = {}
keymovement.threshold = 0.13 -- this effectively controls the speed of the key depress
keymovement.up = 0
keymovement.down = 0
keymovement.left = 0
keymovement.right = 0
keymovement.all = 0

function resetMovement()
    keymovement.up = 0
    keymovement.down = 0
    keymovement.left = 0
    keymovement.right = 0
end

function UPDATE_CONTROLS(dt)
    if love.keyboard.isDown("up") or love.keyboard.isDown("w") then
        keymovement.up = keymovement.up + dt   -- we will increase the variable by 1 for every second the key is held down
        if keymovement.up > keymovement.threshold then
            playerMove("down")
            keymovement.up = 0
        end
    elseif love.keyboard.isDown("down") or love.keyboard.isDown("s") then
        keymovement.down = keymovement.down + dt   -- we will increase the variable by 1 for every second the key is held down
        if keymovement.down > keymovement.threshold then
            playerMove("up")
            keymovement.down = 0
        end
    elseif love.keyboard.isDown("left") or love.keyboard.isDown("a") then
        keymovement.left = keymovement.left + dt   -- we will increase the variable by 1 for every second the key is held down
        if keymovement.left > keymovement.threshold then
            playerMove("left")
            keymovement.left = 0
        end
    elseif love.keyboard.isDown("right") or love.keyboard.isDown("d") then
        keymovement.right = keymovement.right + dt   -- we will increase the variable by 1 for every second the key is held down
        if keymovement.right > keymovement.threshold then
            playerMove("right")
            keymovement.right = 0
        end
    end
end

function love.keyup(key)
    resetMovement()
end

function love.keypressed(key)
    -- reset existing movement timers
    resetMovement()

    -- handleInput
    if(key == "escape") or (key == "q") then
        love.event.quit()
    elseif(key == "r") then
        love.load()
    elseif(key == "w") or (key == "up") then
        --code
        playerMove("down") 
                turn = true
    elseif(key == "a") or (key == "left")then
        --code
        playerMove("left")
                turn = true
    elseif(key == "s") or (key == "down") then
        --code
        playerMove("up")
                turn = true
    elseif(key == "d") or (key == "right")then
        --code
        playerMove("right")
                turn = true
    elseif(key == "m") then
        if(Options.playMusic == true) then
            TEsound.stop("music")
            Options.playMusic = false
        else
            Options.playMusic = true
            TEsound.play(BGM[world.type], "music")
        end
    elseif(key == "i") then
        if(Options.drawDebug == true) then
            Options.drawDebug = false
        else
            Options.drawDebug = true
        end
    elseif(key == "f") then
        --code
        if FullScreen then
            FullScreen = false
        else
            FullScreen = true
        end
        love.load()
    end
end