function enemy_spawn(howMany)
    for x = 1, howMany do
        --print("Initialising actor", x)
        actor = {}
        actor.y = math.random(1, rows)
        actor.x = math.random(1, cols)
        
        if(math.random(1,2) > 1) then
            actor.pronoun = "his"
        else 
            actor.pronoun = "her"
        end
        
            i = math.random(1, #orcNames)
            actor.name = orcNames[i]
        --print("Intitialised",actor.name)
        
           actor.type = math.random(2,3)
        
        if actor.type == 2 then
            maxhealth = 1
            -- goblin
        else 
            maxhealth = 3
            -- ram dude
        end

        actor.hp = math.random(1, maxhealth)
        actor.id = x
        ref = actor.y .. "_" .. actor.x
        
        while map[actor.y][actor.x] > 0 or not not actorMap[ref] do
            actor.y = math.random(1, rows)
            actor.x = math.random(1, cols)
            ref = actor.y .. "_" .. actor.x
            --print("Forloop entered, ref is "..ref)
        end
        
        actor.act_x = actor.x
        actor.act_y = actor.y
        actor.speed = 13
        actor.state = 1 -- 1: idle 2: moved 3: combat 4: burrowing
        
        actorMap[ref] = actor
        table.insert(actorList, actor)
        --print("Actor position is", ref)
        --print()
        livingEnemies = livingEnemies + 1

    end

    if livingEnemies > maxActors then
        maxActorsReached = true
    end

end

function initActors() 
    
    actorList = {}
    actorMap = {}
    
    --print()
    
    for x = 1, actors do
        --print("Initialising actor", x)
        actor = {}
        actor.y = math.random(1, rows)
        actor.x = math.random(1, cols)
        
        if(math.random(1,2) > 1) then
            actor.pronoun = "his"
        else 
            actor.pronoun = "her"
        end
        
        if(x == 1) then 
            i = math.random(1, #playerNames)
            actor.name = playerNames[i]
        else 
            i = math.random(1, #orcNames)
            actor.name = orcNames[i]
        end
        --print("Intitialised",actor.name)
        
        if x == 1 then
            actor.type = 1
        else
            actor.type = math.random(2,3)
        end
        
        if actor.type == 2 then
            maxhealth = 1
            -- goblin
        else 
            maxhealth = 3
            -- ram dude
        end

        actor.statusAlpha = 0

        actor.hp = math.random(1, maxhealth)
        actor.id = x
        ref = actor.y .. "_" .. actor.x
        
        while map[actor.y][actor.x] > 0 or not not actorMap[ref] do
            actor.y = math.random(1, rows)
            actor.x = math.random(1, cols)
            ref = actor.y .. "_" .. actor.x
            --print("Forloop entered, ref is "..ref)
        end
        
        actor.act_x = actor.x
        actor.act_y = actor.y
        actor.speed = 13
        actor.state = 1 -- 1: idle 2: moved 3: combat 4: burrowing
        actor.score = 0
        

        actorMap[ref] = actor
        table.insert(actorList, actor)
        --print("Actor position is", ref)
        --print()
    end
    
    actorList[1].hp = playerHealth -- 10 -- set the HP of the player to 10
    player = actorList[1]
    
    livingEnemies = actors-1
    
    --print("Actors initialised: ", #actorList)
    
end

function calculateVisibleArea(x, y)

    --print("X: "..x.." Y: "..y)

    local visX = 1
    local visY = 2
    local visArea = {}

    local bX1 = x - visX
    local bX2 = x + visX

    local bY1 = y - visY
    local bY2 = y + visY

    --print("bX1: ", bX1)
    --print("bX2: ", bX2)
    --print("bY1: ", bY1)
    --print("bY1: ", bY2)

    for iY = bY1 - 1, bY2 + 1 do

        for iX = bX1 - 2, bX2 + 2 do

            ref = iY .. "_" ..iX
            if not not VisibleSquares[ref] and VisibleSquares[ref] < 150 then
                VisibleSquares[ref] = VisibleSquares[ref] + 4
            end
            
        end

    end

    for iY = bY1 - 2, bY2 + 2 do

        for iX = bX1 - 1, bX2 + 1 do

            ref = iY .. "_" ..iX
            if not not VisibleSquares[ref] and VisibleSquares[ref] < 175 then
                VisibleSquares[ref] = VisibleSquares[ref] + 2
            end
            
        end

    end

    for iY = bY1 - 3, bY2 + 3 do

        for iX = bX1 - 3, bX2 + 3 do

            ref = iY .. "_" ..iX
            if not not VisibleSquares[ref] and VisibleSquares[ref] < 220 then
                VisibleSquares[ref] = VisibleSquares[ref] + 1
            end
            
        end

    end

    for iY = bY1, bY2 do

        for iX = bX1, bX2 do

            ref = iY .. "_" ..iX

            VisibleSquares[ref] = 255
            
        end

    end

    return
end

function updateActors(dt)
    for i,v in ipairs(actorList) do
        --print("Actor " .. i .. " has hp: " .. v.hp)
        if(i == 1) then
            --calculateVisibleArea(v.x, v.y)
        end
        v.act_y = v.act_y - ((v.act_y - v.y) * v.speed * dt)
        v.act_x = v.act_x - ((v.act_x - v.x) * v.speed * dt)

        if not not v.statusAlpha then

            if v.statusAlpha < 1 then
                v.statusAlpha = 0
            else
                v.statusAlpha = v.statusAlpha - (120 * dt)
            end

        else 
            v.statusAlpha = 0
        end

    end
end

function updatePlayer(dt)
    if player.hp > MaxPlayerHealth then
        player.hp = MaxPlayerHealth
    end
end

function drawActors()
    for i,v in ipairs(actorList) do
        
        ref = v.y .. "_" .. v.x
        
        if VisibleSquares[ref] > 74 then
            love.graphics.setColor(255, 255, 255, VisibleSquares[ref])
        else
            love.graphics.setColor(255, 255, 255, 0)
        end 

        if i == 1 then
            love.graphics.draw(playerSprite, v.x * TileSize - TileSize, v.y * TileSize - TileSize)
            calculateVisibleArea(v.x, v.y)
        else 
            if v.type == 3 then
                love.graphics.draw(enemySprites[1], v.x * TileSize - TileSize, v.y * TileSize - TileSize)
            else
                love.graphics.draw(enemySprites[2], v.x * TileSize - TileSize, v.y * TileSize - TileSize)
            end
        end
        
        love.graphics.setColor(255, 255, 255, VisibleSquares[ref])
        love.graphics.setFont(smallFont)

        if VisibleSquares[ref] > 100 and i > 1 then
            --love.graphics.print(v.hp, v.x * TileSize - (TileSize / 2), (v.y * TileSize) - TileSize - 8)
            --love.graphics.print(v.name, v.x * TileSize - TileSize, v.y * TileSize - TileSize)
            love.graphics.setColor(255, 255, 255, v.statusAlpha)
            love.graphics.printf(v.name.." ["..v.hp.."]", (v.x * TileSize) - TileSize * 2, v.y * TileSize - TileSize * 2, 100, "center")
        end
    end
end

function initiateCombat(actor, newKey)
    local loot = {}
    loot.drop = false
    loot.x = 0
    loot.y = 0

    victim = actorMap[newKey]
    vid = table.indexOf(actorList, victim)
    aid = table.indexOf(actorList, actor)
    
    actor.statusAlpha = 255
    victim.statusAlpha = 255

    --print(actor.name.." attacks "..victim.name)
    
    if not vid then
        actorMap[newKey] = nil
        return
    end
    
    --print("Victim ID", vid)
    --print("Aggressor ID", aid)
    
    txt = math.random(1,3)
    
    if math.random(0, 100) > 40 then
        -- A G R E S S O R - H I T
        
        victim.hp = victim.hp - 1
        if actor == player then
            TEsound.play(SFX[2], "sfx")
            actor.score = actor.score + 1
        end

        gameStatus = actor.name .. " attacks " .. victim.name 
        gameStatusAlpha = 255
    else
        -- A G R E S S O R - M I S S 

        actor.hp = actor.hp - 1
        if actor == player then
            TEsound.play(SFX[1], "sfx")
            --actor.score = actor.score - 1
        end

        gameStatus = string.format(txtMisses[txt], actor.name, victim.name)
        gameStatusAlpha = 255
    end

    -- Did anyone die?
    
    if victim.hp < 1 then
        -- A G G R E S S O R - W I N

        actorMap[newKey] = nil
        table.remove(actorList, vid)
        livingEnemies = livingEnemies - 1
        --actor.hp = actor.hp + 1 -- this is now covered by a loot drop
        
        if actor == player then
            -- P L A Y E R  W I N 
            TEsound.play(SFX[3], "sfx")
            actor.score = actor.score + 10
        end
        
        if livingEnemies < 1 then
            actor.score = actor.score + 100
        else
            gameStatus = string.format(txtHits[txt], actor.name, victim.name)
            gameStatusAlpha = 255

            if maxActorsReached == false then
                if maxActors > livingEnemies then
                    -- if there are less enemies than there should be
                    enemy_spawn(2)
                end
            end
            
        end

        loot.drop = true
        loot.x = victim.x
        loot.y = victim.y
    elseif actor.hp < 1 then
        -- A G G R E S S O R - L O S E
        
        newKey = actor.y .. "_" .. actor.x
        actorMap[newKey] = nil
        table.remove(actorList, aid)
        livingEnemies = livingEnemies - 1
        --victim.hp = victim.hp + 2 -- now covered by loot drop
        
        if livingEnemies < 1 then
            
        else
            gameStatus = string.format(txtHits[txt], victim.name, actor.name)
            gameStatusAlpha = 255

            if maxActorsReached == false then
                if maxActors > livingEnemies then
                    -- if there are less enemies than there should be
                    enemy_spawn(2)
                end
            end
            --sound(SOUND_POWERUP, 3302)
        end
        loot.drop = true
        loot.x = actor.x
        loot.y = actor.y
    end

    -- shake the camera
    if actor == player then
        camera:shake(6, 2) -- intensity(px), duration(ms)
    elseif victim == player then
        camera:shake(4, 1)
    end

    -- drop me some loot?

    if loot.drop then
        loot.type = math.random(1,100)

        if loot.type < 70 then
            -- ham chop
            map[loot.y][loot.x] = 9
        elseif loot.type < 90 then
            -- ring
            map[loot.y][loot.x] = 11
        else
            -- bag o booty
            map[loot.y][loot.x] = 10
            
        end
    end
    
    return
end

function initiateBurrowing(actor, x, y)
    -- destroy a block lol

    local terrainloot = true

    if map[y][x] == 9 then
        -- ham chop
        actor.hp = actor.hp + 1
        actor.score = actor.score + 10
        TEsound.play(SFX[5], "sfx")
        gameStatus = actor.name.." nommed on a ham chop"
        gameStatusAlpha = 255
        terrainloot = false
    elseif map[y][x] == 10 then
        -- bag o booty
        actor.hp = actor.hp + 3
        actor.score = actor.score + 100
        TEsound.play(SFX[6], "sfx")

        gameStatus = actor.name.." snaffled a rare bag o' booty"
        gameStatusAlpha = 255

        terrainloot = false        
    elseif map[y][x] == 11 then
        -- ring
        actor.score = actor.score + 200
        TEsound.play(SFX[7], "sfx")

        gameStatus = actor.name.." found the copyright infringement, precioussss"
        gameStatusAlpha = 255

        terrainloot = false
    else

    end
    
    if math.random(1, 100) > 90 and terrainloot then
        -- random loot drop!
        local loottype = math.random(1,100)

        if loottype < 70 then
            -- ham chop
            map[y][x] = 9
        elseif loottype < 90 then
            -- ring
            map[y][x] = 11
        else
            -- bag o booty
            map[y][x] = 10
            
        end

        TEsound.play(SFX[8], "sfx")
    else
        -- no loot drop, boo
        map[y][x] = 0
        TEsound.play(SFX[4], "sfx")
    end
    
    if actor == player then
        camera:shake(0.5, 1)
    end
    
    return
end

function actorMove(dir, actor) 
    moved = false
    oldRef = actor.y .. "_" .. actor.x
    
    if dir == "up" then
        if testMap(actor, 0, -1) then
            actor.y = actor.y + 1
            moved = true
        end
    elseif dir == "down" then
        if testMap(actor, 0, 1) then
            actor.y = actor.y - 1
            moved = true
        end
    elseif dir == "left" then
        if testMap(actor, -1, 0) then
            actor.x = actor.x - 1
            moved = true
        end
    elseif dir == "right" then
        if testMap(actor, 1, 0) then
            actor.x = actor.x + 1
            moved = true
        end
    end
    
    newRef = actor.y .. "_" .. actor.x
    
    if moved then
        actorMap[oldRef] = null
        actorMap[newRef] = actor
    end
    
    return moved
    
end

function aiAct(actor) 
        
    directions = { "left", "right", "up", "down" }

    -- var directions = [ { x: -1, y:0 }, { x:1, y:0 }, { x:0, y: -1 }, { x:0, y:1 } ];
    dx = player.x - actor.x;
    dy = player.y - actor.y;

    --// if player is far away, walk randomly
    if actor.type == 2 then
        maxDistance = 7
        -- goblins should be more aggressive
    else 
        maxDistance = 4
        -- ram dudes care less
    end 

    if livingEnemies < 2 then
        -- the last enemy will come for you
        maxDistance = 100
    end
    
    if (math.abs(dx) + math.abs(dy) > maxDistance) then
        --while (!moveTo(actor, directions[randomInt(directions.length)])) { };
        --print("Player too far from", actor.name)
        while(not actorMove(directions[math.random(1,#directions)], actor)) do end
    else
        --otherwise walk towards player
        if (math.abs(dx) > math.abs(dy)) then
            if (dx < 0) then
                -- left
                actorMove(directions[1], actor);

            else
                -- right
                actorMove(directions[2], actor);
            end
        else
            if (dy < 0) then
                -- up
                -- switched to down to compensate for codea's weird up down thing
                -- reswitched to up in love2d
                actorMove(directions[4], actor);
            else
                -- down
                -- switched to up to compensate for codea's weird up down thing
                -- reswitched to down in love2d
                actorMove(directions[3], actor);
            end
        end
        
        --print("Player attracts", actor.name)

        if (player.hp < 1) then
            --print("Game technically over. Um.")    
            gameStatus = player.name .. " died in " .. world.name
            gameStatusAlpha = 255
        end

    end
        
end

function playerMove(dir) 
    moved = 0
    
    oldRef = player.y .. "_" .. player.x
    
    if dir == "up" then
        if testMap(player, 0, -1) then
            player.y = player.y + 1
            moved = 1
        end
    elseif dir == "down" then
        if testMap(player, 0, 1) then
            player.y = player.y - 1
            moved = 1
        end
    elseif dir == "left" then
        if testMap(player, -1, 0) then
            player.x = player.x - 1
            moved = 1
        end
    elseif dir == "right" then
        if testMap(player, 1, 0) then
            player.x = player.x + 1
            moved = 1
        end
    end

    newRef = player.y .. "_" .. player.x
    
    actorList[1] = player
    
    if moved == 1 then
        
        --print("Player moved  to", newRef)
        
        actorMap[oldRef] = null
        actorMap[newRef] = player
        
        --sound(SOUND_BLIT, 22418)
        
    else
        --sound(DATA, "ZgBAHwBAQlZAQEBCAAAAAP/YHjzVNA0+AgBAfj9FQEBAPEJB")
    end

    for i,v in ipairs(actorList) do 
        --skip the player
        if(i > 1) then
            e = actorList[i];
            if (not not e) then
                aiAct(e)
            end
        end        
    end
    
end

function testMap(actor, x, y)
    newKey = (actor.y - y) .. '_' ..  (actor.x + x);
    
    if not not actorMap[newKey] then
        initiateCombat(actor, newKey)
        return false
    elseif actor.y - y < 1 or actor.y - y > rows or actor.x + x < 1 or actor.x + x > cols then
        --print("Boundary reached")
        return false
    elseif map[actor.y - y][actor.x + x] > 0 then

        if actor == player then
            initiateBurrowing(actor, actor.x + x, actor.y - y)
            actor.state = 4 -- 1: idle 2: moved 3: combat 4: burrowing
        end
        return false
    end
    
    
    return true
end