function love.conf(t)
    io.stdout:setvbuf("no")
    t.title = "Bring it! The world's no 1 goblin genocide simulator."        -- The title of the window the game is in (string)
    t.author = "Anguaji"        -- The author of the game (string)
    t.url = "http://anguaji.net"                 -- The website of the game (string)
    
    --t.screen.fullscreen = true -- Enable fullscreen (boolean)
    --t.screen.width = 1440 -- t.screen.width in 0.8.0 and earlier
    --t.screen.height = 900 -- t.screen.height in 0.8.0 and earlier
    print("Configured and ready to go!")
end