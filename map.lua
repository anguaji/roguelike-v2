function initMap()
    -- create a new random map
    map = {}
    decalMap = {}
    w = 0
    f = 0
                        
    for y = 1, rows do
        newRow = {}
        decalRow = {}

        for x = 1, cols do   
            if(math.random(0,100) > chanceToBeAlive) then
                if(math.random(0,100) > 80) then
                    -- wall variation 1
                    table.insert(newRow, 1)
                else
                    -- wall variation 2
                    table.insert(newRow, 2)
                end
                w = w + 1
            else
                table.insert(newRow, 0)
                f = f + 1
            end

            ref = y .. "_" .. x
            VisibleSquares[ref] = InitVisibility
            table.insert(decalRow, 0)
        end
        table.insert(map, newRow)
        table.insert(decalMap, decalRow)
    end
    --print("Initialised map with "..f.." floor space and "..w.." walls")
end

function drawMap()
    
    for y=1, #map do
        for x=1, #map[y] do

            ref = y .. "_" .. x
            
            love.graphics.setColor(255, 255, 255, VisibleSquares[ref])
            
            -- draw ground
            love.graphics.draw(world.mapSprites[1], x * TileSize - TileSize, y * TileSize - TileSize, 0, ScaleFactor, ScaleFactor)
            
            if Options.showBlood then
                if decalMap[y][x] > 0 then
                    love.graphics.setColor(255, 255, 255, VisibleSquares[ref])
                
                    local bld = decalMap[y][x]
                    love.graphics.draw(decalSprites[bld], x * TileSize - TileSize, y * TileSize - TileSize, 0, ScaleFactor, ScaleFactor)
                end            
            end
            
            love.graphics.setColor(255, 255, 255, VisibleSquares[ref])

            if map[y][x] == 1 then
                -- wall 1
                love.graphics.draw(world.mapSprites[2], x * TileSize - TileSize, y * TileSize - TileSize, 0, ScaleFactor, ScaleFactor)
            elseif map[y][x] == 2 then
                -- wall 2
                love.graphics.draw(world.mapSprites[3], x * TileSize - TileSize, y * TileSize - TileSize, 0, ScaleFactor, ScaleFactor)
            elseif map[y][x] == 9 then
                -- loot - ham chop
                love.graphics.draw(lootSprite[1], x * TileSize - TileSize, y * TileSize - TileSize, 0, ScaleFactor, ScaleFactor)
            elseif map[y][x] == 10 then
                -- loot - bag o booty
                love.graphics.draw(lootSprite[2], x * TileSize - TileSize, y * TileSize - TileSize, 0, ScaleFactor, ScaleFactor)
            elseif map[y][x] == 11 then
                -- loot - ring
                love.graphics.draw(lootSprite[3], x * TileSize - TileSize, y * TileSize - TileSize, 0, ScaleFactor, ScaleFactor)
            elseif map[y][x] == 0 then
                -- empty space
            end
            
            dummyRef = y .. "_" .. x

            if not not actorMap[dummyRef] then
                --print("Actor at", dummyRef)
                --rect(x * TileSize - TileSize , y * TileSize - TileSize , TileSize + 5, TileSize + 5)
            end
           
        end
    end 
end

-- Cellular Automata

function doSimulation(oldMap)
    -- will return the map, altered
    --print("Running simulation")
    newMap = oldMap
    
    for y = 1, #oldMap do
        for x = 1, #oldMap[y] do
            --print("Tile at "..x..","..y.." is "..oldMap[y][x])
            nbs = countAliveNeighbours(oldMap, x, y)
            --print("NBS at "..x..","..y..":"..nbs)
            
            if oldMap[y][x] == 1 then
                if nbs < deathLimit then
                    newMap[y][x] = 0 -- kill it
                    --print("Killed cell at "..x..","..y)
                else 
                    newMap[y][x] = oldMap[y][x]
                    --print("Left cell at "..x..","..y)
                end
            else
                if nbs > birthLimit then
                    if(math.random(0,100) > 80) then
                        newMap[y][x] = 1
                    else
                        newMap[y][x] = 2
                    end
                    --print("Birthed cell at "..x..","..y)
                else 
                    newMap[y][x] = 0 -- leave it
                    --print("Left cell at "..x..","..y)
                end
            end
        end
    end
    
    print("Simulation ended")
    return newMap;
    
    --newMap = oldMap
    --return newMap
end

function countAliveNeighbours(myMap, x, y)
    --print("myMap received upper limit y: ", #myMap)
    count = 0
    
    for i= 0, 1 do
        for j= 0, 1 do
            
            neighbour_x = x + i
            neighbour_y = y + j
            
            if i == 0 and j == 0 then
                --print("Looking at middle point! "..x..","..y)
            elseif neighbour_y > #myMap then
                --print("Y exceeded:", neighbour_y)
            elseif neighbour_x > #myMap[y] then 
                --print("x exceeded:", neighbour_x)
            elseif neighbour_x < 1 or neighbour_y < 1 then
                --print("x or y less than 1")
            elseif not not myMap[neighbour_y][neighbour_x] and myMap[neighbour_y][neighbour_x] > 0 then
                count = count + 1
                --print("Count:", count)                
            else
               -- print("Not count")
            end
        end
    end
    
    return count
end