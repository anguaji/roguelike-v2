function gui_update(dt) 
    
    if gameStatusAlpha > 1 then

       if gameStatusAlpha < 170 then
            gameStatusAlpha = gameStatusAlpha - (180 * dt)
        else
            gameStatusAlpha = gameStatusAlpha - (50 * dt)
        end
    end
    
    if gameStatusAlpha < 1 then
        gameStatusAlpha = 0
    end
end

function gui_drawHealth()
    local x = 10
    local y = HEIGHT - (TileSize * 2) + 10

    for i = 1, player.hp do
        --sprite("Dropbox:heart", x, 32)
        love.graphics.draw(heartSprite, x, y)
        x = x + 20
    end
end

function gui_drawDebug()
    local y = 10 --HEIGHT - 10
    local x = 0 --10

    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.setFont(smallFont)
    
    love.graphics.print("Keymovement up: "..keymovement.up, 0, y + 0)
    love.graphics.print("Keymovement down: "..keymovement.down, 0, y + 10)
    love.graphics.print("Keymovement left: "..keymovement.left, 0, y + 20)
    love.graphics.print("Keymovement right: "..keymovement.right, 0, y + 30)
    love.graphics.print("ScaleFactor is:"..ScaleFactor, 0, y + 40)
    love.graphics.print("FPS: "..love.timer.getFPS(), 0, y + 50)
    love.graphics.print("Player Hit Status: "..player.hitStatus, 0, y + 60)
    love.graphics.print("gameStatusAlpha: "..gameStatusAlpha, 0, y + 70)
    love.graphics.print("act_x: "..player.act_x, 0, y + 80)
    love.graphics.print("act_y: "..player.act_y, 0, y + 90)
end

function gui_drawEnemies()
    local x = 10
    local y = HEIGHT - TileSize

    for i,v in ipairs(actorList) do
        if i == 1 then
            love.graphics.draw(playerSprite, x, y, 0, 0.7, 0.7)
        else 
            if v.type == 3 then
                love.graphics.draw(enemySprites[1], x, y, 0, 0.7, 0.7)
            else
                love.graphics.draw(enemySprites[2], x, y, 0, 0.7, 0.7)
            end
        end
        x = x + (TileSize / 1.3)
    end
end

function gui_drawGui()
    love.graphics.setColor(255, 255, 255)
    
    -- get down to business
    TransX = math.floor((WIDTH - (TileSize * cols)) / 2) + 1
    TransY = math.floor((HEIGHT - (TileSize * rows)) / 2) + 1
    
    gui_drawEnemies()
    gui_drawHealth()
    
    love.graphics.setColor(0, 0, 0, gameStatusAlpha / 2)
    love.graphics.rectangle("fill", 0, 20, WIDTH, 80)


    love.graphics.setColor(255, 255, 255, gameStatusAlpha)
    love.graphics.setFont(largeFont)
    love.graphics.printf(gameStatus, 0, 0 + 24, WIDTH, "center")

    love.graphics.setColor(255, 255, 255)
    
    love.graphics.setFont(largeFont)
    love.graphics.printf("Score: "..player.score, 0, HEIGHT - 80, WIDTH, "center")
    
    if Options.drawDebug then
        gui_drawDebug()
    end 

    if miniMapShow then
        gui_drawMiniMap(miniMapOriginX, miniMapOriginY)
    end 
end

function gui_drawMiniMap(originX, originY)

    TileScale = MiniMapScale --math.floor(TileSize / 8)
    
    love.graphics.translate(originX, originY)
    
    local alpha = 255

    local newHeight = rows * TileScale + 50 -- (542)
    local newWidth = cols * TileScale + 50 -- (542)

    local originalHeight = itemSprites[1]:getHeight()
    local originalWidth = itemSprites[1]:getWidth()

    local scaleFactor = newWidth / originalWidth

    love.graphics.setColor(255, 255, 255, 255)
    --love.graphics.rectangle("line", -1, -1, cols * TileScale + 2, rows * TileScale + 2)
    love.graphics.draw(itemSprites[1], -20, -20, 0, scaleFactor, scaleFactor)
           
    love.graphics.setColor(255, 255, 255, alpha)
    love.graphics.translate(15, 15)
    
    for y=1, #map do
        for x=1, #map[y] do
            --love.graphics.draw(world.mapSprites[1], x * TileScale - TileScale, y * TileScale - TileScale, 0, SpriteScale, SpriteScale)
            if map[y][x] == 1 then
                -- wall 1
                love.graphics.draw(world.mapSprites[2], x * TileScale - TileScale, y * TileScale - TileScale, 0, SpriteScale, SpriteScale)
            elseif map[y][x] == 2 then
                -- wall 2
                love.graphics.draw(world.mapSprites[3], x * TileScale - TileScale, y * TileScale - TileScale, 0, SpriteScale, SpriteScale)
            elseif map[y][x] > 8 and map[y][x] < 12 then
                -- loot
                love.graphics.setColor(74, 217, 47, alpha)
                love.graphics.setFont(smallFont)
                love.graphics.print("*", x * TileScale - TileScale, y * TileScale - TileScale)
                love.graphics.setColor(255, 255, 255, alpha)
            elseif map[y][x] == 0 then
                
            end
            
        end
    end 

    for i,v in ipairs(actorList) do
        love.graphics.setColor(255,255,255, alpha)
        love.graphics.setFont(smallFont)
        if i == 1 then
            love.graphics.setColor(255, 255, 255, alpha)
            love.graphics.print("*", v.x * TileScale - TileScale, v.y * TileScale - TileScale)
            --love.graphics.rectangle("fill", v.x * TileScale - TileScale, v.y * TileScale - TileScale, TileScale, TileScale)
            --love.graphics.draw(playerSprite, v.x * TileScale - TileScale, v.y * TileScale - TileScale, 0, SpriteScale, SpriteScale)
        else 
            if v.type == 3 then
                love.graphics.setColor(255, 0, 0, alpha)
                --love.graphics.rectangle("fill", v.x * TileScale - TileScale, v.y * TileScale - TileScale, TileScale, TileScale)
                love.graphics.print("*", v.x * TileScale - TileScale, v.y * TileScale - TileScale)
            --love.graphics.draw(enemySprites[1], v.x * TileScale - TileScale, v.y * TileScale - TileScale, 0, SpriteScale, SpriteScale)
            else
                love.graphics.setColor(255, 0, 0, alpha)
                --love.graphics.rectangle("fill", v.x * TileScale - TileScale, v.y * TileScale - TileScale, TileScale, TileScale)
                love.graphics.print("*", v.x * TileScale - TileScale, v.y * TileScale - TileScale)
            --love.graphics.draw(enemySprites[2], v.x * TileScale - TileScale, v.y * TileScale - TileScale, 0, SpriteScale, SpriteScale)
            end
        end
        
        love.graphics.setColor(255, 255, 255, 255)
        
    end
     
end