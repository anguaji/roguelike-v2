--------------------------
--    ROGUELIKE*LIKE    --
--    v2 Camera         --
--------------------------

-- commit: 63539fb221a243434a3b8e70f506b0e61b7ae2b3

require "map"
require "actors"
require "TESound"
require "camera"
require "texts"
require "CameraShake"
require "controls"
require "gui"
require "effects"

function round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end

function approach(goal, current, dt)
    local difference = goal - current

    if difference > dt then 
        return current + dt
    end

    if difference < -dt then
        return current  - dt
    end

    return goal
end 

function math.clamp(x, min, max)
  return x < min and min or (x > max and max or x)
end

table.indexOf = function( t, object )
    local result
 
    if "table" == type( t ) then
        for i=1,#t do
            if object == t[i] then
                result = i
                break
            end
        end
    end
 
    return result
end

---------------------------------
-- G L O B A L S ----------------
---------------------------------

FullScreen = false
VitaMode = false
playMusic = true

function love.load()

    -- SET UP DAT RANDOM
    math.randomseed(os.time())
    math.random()
    math.random()
    math.random()
	
    -- declare globals
    --love.graphics.setMode(800, 600)

    if VitaMode == true then
        love.window.setMode(960, 544)
    elseif FullScreen == false then
        love.window.setMode(1024, 720)
    else    
        love.window.setFullscreen(true, "desktop")
    end

    --love.window.setMode(1024, 768)
    --love.window.setFullscreen(true, "desktop")
    
    Timer = {}

	WIDTH = love.graphics.getWidth()
    HEIGHT = love.graphics.getHeight()
    
    love.graphics.setDefaultFilter('nearest')
    
    -- set up fonts
    largeFont = love.graphics.newFont("PressStart2P.ttf", 11)
    font = love.graphics.newFont("PressStart2P.ttf", 9)
    smallFont = love.graphics.newFont("PressStart2P.ttf", 8)
    largeFont = love.graphics.newFont("AmaticSC-Regular.ttf", 48)

    InitVisibility = 0
    OriginalTileSize = 32 -- original tilesize

    cols = (math.floor(WIDTH/OriginalTileSize) * 1) --20
    rows = cols -- (math.floor(HEIGHT/TileSize) * 2)  --20

    TileSize = WIDTH / cols -- new tilesize
    ScaleFactor = TileSize / OriginalTileSize
    
    actors = 3
    maxActors = math.floor((rows * cols) / 130) * 2 -- adjusted for balancing
    maxActorsReached = false

    playerHealth = math.floor(actors / 3)

    if playerHealth < 10 then
        playerHealth = 10
    end

    --playerHealth = 2 -- this is for debugging death! :D
    
    miniMapShow = false
    MiniMapScale = math.floor(TileSize / 3)
    SpriteScale = 1 / (TileSize / MiniMapScale)

    miniMapOriginX = WIDTH - (cols * MiniMapScale) - 50
    miniMapOriginY = HEIGHT - (rows * MiniMapScale) - 50

    MaxPlayerHealth = 10

    --print("Minimap scale is ", MiniMapScale)
    --print("Sprite scale is ", SpriteScale)
    chanceToBeAlive = 100 - 80
    mapIterations = 3
    birthLimit = 2
    deathLimit = 2

    cameraBounds = {}
    cameraBounds.x1 = 0
    cameraBounds.y1 = 0
    cameraBounds.x2 = cols * TileSize - (WIDTH)
    cameraBounds.y2 = rows * TileSize - (HEIGHT)

    print("World is " .. cols * TileSize .. "px x " .. rows * TileSize .. "px")

    camera:setBounds(0, 0, cameraBounds.x2, cameraBounds.y2)
    camera:setScale(1, 1)

    map = {} -- forms the playing grid
    decalMap = {} -- added to support decals on the terrain
    VisibleSquares = {}

    -- actor variables
    player = {}
    actorList = {}
    livingEnemies = {}
    playerisalive = true

    player_speed = 8
    ram_speed = 7
    goblin_speed = 15

    -- point to actor locations for quick locating
    actorMap = {}

    initMap() -- generates the terrain
    
    for i = 1, mapIterations do
        map = doSimulation(map)
    end

    initActors()
    
    i = math.random(1, #worldNames)
    t = math.random(1, 4) -- 1: Mountain 2: Desert 3: Forest 4: Snow
    --t = 4 -- testing snow

    world = {
        type = t,
        name = worldNames[i]
    }
    
    print("World is a "..worldTypes[world.type])

    terrain = {
        {
            "assets/col-floor-3.png",
            "assets/col-wall-2.png",
            "assets/col-wall-5.png"
        },
        {
            "assets/col-floor-2.png",
            "assets/col-wall-2.png",
            "assets/col-wall-5.png"
        },
        {
            "assets/col-floor-1.png",
            "assets/col-wall-1.png",
            "assets/col-wall-1.png"
        },
        {
            "assets/col-floor-snow-1.png",
            "assets/col-wall-5.png",
            "assets/col-wall-2.png"
        },
    } -- 1: floor 2: wall 1 3: wall 2
    
    world.terrain = terrain[world.type]

    world.mapSprites = {} 
    world.mapSprites[1] = love.graphics.newImage(world.terrain[1])
    world.mapSprites[2] = love.graphics.newImage(world.terrain[2])
    world.mapSprites[3] = love.graphics.newImage(world.terrain[3])
    
    enemySprites = {}
    enemySprites[1] = love.graphics.newImage("assets/col-actor-5.png")
    enemySprites[2] = love.graphics.newImage("assets/col-actor-9.png")
    
    playerSprite = love.graphics.newImage("assets/col-actor-4.png")

    itemSprites = {}
    itemSprites[1] = love.graphics.newImage("assets/map-shadow.png")
    
    decalSprites = {}
    decalSprites[1] = love.graphics.newImage("assets/blood-1.png")
    decalSprites[2] = love.graphics.newImage("assets/blood-2.png")
    decalSprites[3] = love.graphics.newImage("assets/blood-3.png")
    decalSprites[4] = love.graphics.newImage("assets/blood-4.png")
    
    i = math.random(1, #startings)
    --for i,v in pairs(world.mapSprites) do print(i,v) end
    
    gameStatusAlpha = 255
    gameStatus = player.name .. " made " .. player.pronoun .. " way into " .. world.name ..", ready to " .. startings[i]
    --print(gameStatus)
    
    Options = {}
    Options.drawDebug = false
    Options.playMusic = true
    Options.showBlood = true
 	-- sprites   
    heartSprite = love.graphics.newImage("assets/heart.png")

    lootSprite = {} -- loot sprites array
    lootSprite[1] = love.graphics.newImage("assets/loot-1.png") -- 9
    lootSprite[2] = love.graphics.newImage("assets/loot-2.png")
    lootSprite[3] = love.graphics.newImage("assets/loot-3.png")

    deathScreen = love.graphics.newImage("assets/you-lose.png")
    winScreen = love.graphics.newImage("assets/you-win.png")

    dscx = WIDTH / 2 - (deathScreen:getWidth() / 2)
    dscy = HEIGHT / 2 - (deathScreen:getHeight() / 2)

    wscx = WIDTH / 2 - (winScreen:getWidth() / 2)
    wscy = HEIGHT / 2 - (winScreen:getHeight() / 2)
    
    love.graphics.setFont(font)
    camera:setPosition((player.act_x * TileSize) - WIDTH / 2, (player.act_y * TileSize) - HEIGHT / 2)

    TEsound.stop("music")
    BGM = {"assets/bgm/07-boc-meljuice.mp3", "assets/bgm/02-boc-turq.mp3", "assets/bgm/05-boc-seeya.mp3", "assets/bgm/01-mad-guida.mp3", "assets/bgm/static.ogg"}
    
    if playMusic then
        TEsound.play(BGM[world.type], "music")
    end

    SFX = {
        "assets/sfx/playerHit.wav", 
        "assets/sfx/enemyHit.wav", 
        "assets/sfx/enemyDie.wav", 
        "assets/sfx/burrow.wav",
        "assets/sfx/healthup.wav",
        "assets/sfx/bootyup.wav",
        "assets/sfx/ringup.wav",
        "assets/sfx/terrainlootup.wav"
    }
    
    particleSprite = love.graphics.newImage("assets/particles/particle.png")
    particles = love.graphics.newParticleSystem(particleSprite, 4.5 )
    particles:setPosition(player.x * TileSize + TileSize / 2 - TileSize, player.y * TileSize - (TileSize / 2))
    particles:setOffset( 0, 0 )
    particles:setBufferSize( 2000 )
    particles:setEmissionRate( 30 )
    particles:setEmitterLifetime( 0.2 )
    particles:setParticleLifetime( 1.2 )
    particles:setColors( 255, 255, 20, 255, 255, 255, 0, 100 )
    particles:setColors( 255, 255, 255, 255, 255, 255, 255, 100 )
    particles:setSizes( 1, 2, 0 )
    particles:setSpeed( 50, 110  )
    particles:setDirection( math.rad(270) )
    particles:setSpread( math.rad(360) )
    particles:setLinearAcceleration( 0, 0 )
    particles:setRotation( math.rad(0), math.rad(0) )
    particles:setSpin( math.rad(0), math.rad(0), 1 )
    particles:setRadialAcceleration( 0 )
    particles:setTangentialAcceleration( 0 )

end

function love.update(dt)
	
    if playerisalive then
        UPDATE_CONTROLS(dt) -- now exists in controls.lua
    
        camera:update(dt) -- now exists in camera.lua

        if livingEnemies < 1 then
            gameStatus = player.name .. " vanquished all " .. player.pronoun .. " enemies!!"
            gameStatusAlpha = 255
        end

        if(player.hp < 1) then 
            gameStatus = player.name .. " lost " .. player.pronoun .. " life causing a ruckus in " .. world.name
            gameStatusAlpha = 255
            --drawMap()
        end

        updateActors(dt)
        updatePlayer(dt)

        if love.keyboard.isDown( " " ) then
            miniMapShow = true
        else
            miniMapShow = false
        end
        
        gui_update(dt)

    end

    effects.update(dt)
    particles:update(dt)
    --camera:setPosition((player.x * TileSize) / 2, (player.y * TileSize) / 2)
    --camera:setPosition(player.x - WIDTH / 2, player.y - HEIGHT / 2)
end



function love.draw()
    
    --love.graphics.setBackgroundColor(255, 255, 255, 255)

    -- A C T I O N

    camera:set()
        --love.graphics.setShader(MyShader)
        drawMap()
        effects.draw()
        love.graphics.draw(particles, 0, 0);
        if player.hp > 0 then
            drawActors()
        end
        
        --love.graphics.setShader()
    camera:unset()

    if(player.hp > 0) then 
        if livingEnemies < 1 then
            love.graphics.setColor(255, 255, 255, gameStatusAlpha)
            love.graphics.draw(winScreen, wscx, wscy)
        end
    else
        love.graphics.setColor(255, 255, 255, gameStatusAlpha)
        love.graphics.draw(deathScreen, dscx, dscy)
    end

    -- G U I

    gui_drawGui()



end

function love.quit()
  --print("Thanks for playing! Come back soon!")
end

function delay_s(delay)
  delay = delay or 1
  local time_to = os.time() + delay
  while os.time() < time_to do end
end